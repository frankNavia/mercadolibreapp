//
//  Currency.swift
//  MercadoLibreApp
//
//  Created by PersonalSoft PS on 10/04/21.
//

import Foundation

protocol CurrencyView: NSObjectProtocol {
    func startActivity()
    func pushError(_ messageError:String)
    func pushDataView(_ currency:CurrencyModel)
}
