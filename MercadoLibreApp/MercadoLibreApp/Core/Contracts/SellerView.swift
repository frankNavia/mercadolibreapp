//
//  SellerView.swift
//  MercadoLibreApp
//
//  Created by PersonalSoft PS on 10/04/21.
//

import Foundation

protocol SellerView: NSObjectProtocol {
    func startActivity()
    func pushError(_ messageError:String)
    func pushDataView(_ seller:SellerModel)
}
