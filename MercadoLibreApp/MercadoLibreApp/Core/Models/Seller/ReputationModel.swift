//
//  ReputationModel.swift
//  MercadoLibreApp
//
//  Created by PersonalSoft PS on 10/04/21.
//

import Foundation

struct ReputationModel {
    let status: String
    let canceledTransactions, completedTransactions, totalTransactions: Int
    let negativeRating, neutralRating, positiveRating: Double
}
