//
//  SellerItemModel.swift
//  MercadoLibreApp
//
//  Created by PersonalSoft PS on 10/04/21.
//

import Foundation

struct SellerModel {
    let id, points: Int
    let name, countryID, permalink: String
    let tags: [String]
    let reputation: ReputationModel
    var logo: String?
}
