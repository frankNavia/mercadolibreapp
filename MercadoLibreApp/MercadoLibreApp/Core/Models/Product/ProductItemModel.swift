//
//  ProductItem.swift
//  MercadoLibreApp
//
//  Created by PersonalSoft PS on 10/04/21.
//

import Foundation

struct ProductItemModel {
    let id, title, currencyID, categoryID, condition, address: String
    let mercadoLink, thumbnail: String
    let availableQuantity, soldQuantity, sellerID: Int
    let price: Double
    let acceptsMercadopago: Bool
    var catalogProductID: String?
    var officialStoreID: Int?
    var attributes: [String]?
}
