//
//  SelleRepository.swift
//  MercadoLibreApp
//
//  Created by PersonalSoft PS on 10/04/21.
//

import Foundation

class SellerRepository : BaseRepository {

    func getSellerFromId(id: Int, userCompletionHandler: @escaping(_ responseData: SellerModel?,_ messageError: String?)->Void) {
        let endPont = String(format: endPoint.sellerDataEndPoint, "\(id)")
        let url = endPoint.urlBase + endPont
        
        let session = URLSession.shared
        var request = URLRequest(url: URL(string: url)!)
        request.httpMethod = "GET"
        request.setValue("application/json; charset=utf8", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")

        session.dataTask(with: request) {(data,response,error) in
            guard let data = data, error == nil, let result = response as? HTTPURLResponse else {
                userCompletionHandler(nil, "Error interno del servidor")
                return
            }

            if (result.statusCode == 200) {
                do {
                    let decoder = JSONDecoder()
                    let rsSeller = try decoder.decode(RsSeller.self, from: data)
                    
                    let rsReputation = rsSeller.sellerReputation
                    let rsTransaction = rsReputation.transactions
                    let rsRating = rsTransaction.ratings
                    let statusSeller = rsReputation.powerSellerStatus ?? "Sin Estado"
                    let reputationModel = ReputationModel(status: statusSeller, canceledTransactions: rsTransaction.canceled, completedTransactions: rsTransaction.completed, totalTransactions: rsTransaction.total, negativeRating: rsRating.negative, neutralRating: rsRating.neutral, positiveRating: rsRating.positive)
                    
                    var sellerModel = SellerModel(id: rsSeller.id, points: rsSeller.points, name: rsSeller.nickname, countryID: rsSeller.countryID, permalink: rsSeller.permalink, tags: rsSeller.tags, reputation: reputationModel)
                    sellerModel.logo = rsSeller.logo
                    
                    userCompletionHandler(sellerModel, nil)
                } catch {
                    print(error)
                    userCompletionHandler(nil, error.localizedDescription)
                }
            } else {
                userCompletionHandler(nil, "Error de comunicación")
            }
        }.resume()
    }
    
 }
