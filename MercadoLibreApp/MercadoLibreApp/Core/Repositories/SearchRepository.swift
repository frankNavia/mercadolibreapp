//
//  SearchItem.swift
//  MercadoLibreApp
//
//  Created by PersonalSoft PS on 10/04/21.
//

import Foundation

class SearchRepository : BaseRepository {

    func searchProduct(text: String, userCompletionHandler: @escaping(_ responseData: [ProductItemModel]?,_ messageError: String?)->Void) {
        let newtext = text.replacingOccurrences(of: " ", with: "%20")
        let endPont = String(format: endPoint.searchEndPoint, newtext)
        let url = endPoint.urlBase + endPont.folding(options: .diacriticInsensitive, locale: .current)
        
        let session = URLSession.shared
        var request = URLRequest(url: URL(string: url)!)
        request.httpMethod = "GET"
        request.setValue("application/json; charset=utf8", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")

        session.dataTask(with: request) {(data,response,error) in
            guard let data = data, error == nil, let result = response as? HTTPURLResponse else {
                userCompletionHandler(nil, "Error interno del servidor")
                return
            }

            if (result.statusCode == 200) {
                do {
                    let decoder = JSONDecoder()
                    let rsSearch = try decoder.decode(RsSearch.self, from: data)
                    if rsSearch.results.isEmpty {
                        userCompletionHandler(nil, "No hay coincidencia de datos")
                    } else {
                        var products = [ProductItemModel]()
                        for data in rsSearch.results {
                            let conditions = data.condition ?? "Used"
                            let addressName = "\(data.sellerAddress.city.name), \(data.sellerAddress.state.name), \(data.sellerAddress.country.name)"
                            
                            var item = ProductItemModel(id: data.id, title: data.title, currencyID: data.currencyID, categoryID: data.categoryID, condition: conditions, address: addressName, mercadoLink: data.permalink, thumbnail: data.thumbnail, availableQuantity: data.availableQuantity, soldQuantity: data.soldQuantity, sellerID: data.seller.id, price: data.price, acceptsMercadopago: data.acceptsMercadopago)
                            item.attributes = self.getAttributes(attributes: data.attributes)
                            item.catalogProductID = data.catalogProductID
                            item.officialStoreID = data.officialStoreID
                            
                            products.append(item)
                        }
                        
                        userCompletionHandler(products, nil)
                    }
                } catch {
                    userCompletionHandler(nil, error.localizedDescription)
                }
            } else {
                userCompletionHandler(nil, "Error de comunicación")
            }
        }.resume()
    }
    
    private func getAttributes(attributes: [RsAttribute]!) -> [String] {
        var attributesArr = [String]()
        for item in attributes {
            if item.valueName != nil {
                let label = "\(item.name): \(item.valueName!)"
                attributesArr.append(label)
            }
        }
        
        return attributesArr
    }
    
 }
