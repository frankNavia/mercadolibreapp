//
//  CurrencyRespository.swift
//  MercadoLibreApp
//
//  Created by PersonalSoft PS on 10/04/21.
//

import Foundation

class CurrencyRepository : BaseRepository {
  
    func convertPriceToCurrency(currency: String, userCompletionHandler: @escaping(_ responseData: CurrencyModel?,_ messageError: String?)->Void) {
        let endPont = String(format: endPoint.currencyConvertEndPoint, currency)
        let url = endPoint.urlBase + endPont
        
        let session = URLSession.shared
        var request = URLRequest(url: URL(string: url)!)
        request.httpMethod = "GET"
        request.setValue("application/json; charset=utf8", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")

        session.dataTask(with: request) {(data,response,error) in
            guard let data = data, error == nil, let result = response as? HTTPURLResponse else {
                userCompletionHandler(nil, "Error interno del servidor")
                return
            }

            if (result.statusCode == 200) {
                do {
                    let decoder = JSONDecoder()
                    let rsCurrency = try decoder.decode(RsCurrencyConvert.self, from: data)
                    let currencyModel = CurrencyModel(ratio: rsCurrency.ratio)
                    userCompletionHandler(currencyModel, nil)
                } catch {
                    userCompletionHandler(nil, error.localizedDescription)
                }
            } else {
                userCompletionHandler(nil, "Error de comunicación")
            }
        }.resume()
    }
    
}
