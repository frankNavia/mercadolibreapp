//
//  SearchPresenter.swift
//  MercadoLibreApp
//
//  Created by PersonalSoft PS on 10/04/21.
//

import Foundation

class SearchPresenter {

    let service:SearchRepository
    weak fileprivate var searchView: SearchView?
    
    init() {
        service = SearchRepository()
    }
    
    func attachView(view: SearchView?) {
        searchView = view
    }
    
    func searchProducts(text: String) {
        self.searchView?.startActivity()
        
        service.searchProduct(text: text, userCompletionHandler: { (response, error) in
            if (error != nil) {
                self.searchView?.pushError(error!)
                return
            } else {
                print(response!.count as Int)
                self.searchView?.pushDataView(response!)
            }
        })
    }
    
}
