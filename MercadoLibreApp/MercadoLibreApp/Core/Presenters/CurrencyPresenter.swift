//
//  CurrencyPresenter.swift
//  MercadoLibreApp
//
//  Created by PersonalSoft PS on 10/04/21.
//

import Foundation

class CurrencyPresenter {

    let service:CurrencyRepository
    weak fileprivate var currencyView: CurrencyView?
    
    init() {
        service = CurrencyRepository()
    }
    
    func attachView(view: CurrencyView?) {
        currencyView = view
    }
    
    func convertPriceToCurrency(currencyID: String) {
        self.currencyView?.startActivity()
        
        service.convertPriceToCurrency(currency: currencyID, userCompletionHandler: { (response, error) in
            if (error != nil) {
                self.currencyView?.pushError(error!)
                return
            } else {
                self.currencyView?.pushDataView(response!)
            }
        })
    }
}
