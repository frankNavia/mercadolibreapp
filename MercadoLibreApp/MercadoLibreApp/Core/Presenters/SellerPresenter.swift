//
//  SellerPresenter.swift
//  MercadoLibreApp
//
//  Created by PersonalSoft PS on 10/04/21.
//

import Foundation

class SellerPresenter {

    let service:SellerRepository
    weak fileprivate var sellerView: SellerView?
    
    init() {
        service = SellerRepository()
    }
    
    func attachView(view: SellerView?) {
        sellerView = view
    }
    
    func getSellerFromID(sellerID: Int) {
        self.sellerView?.startActivity()
        
        service.getSellerFromId(id: sellerID, userCompletionHandler: { (response, error) in
            if (error != nil) {
                self.sellerView?.pushError(error!)
                return
            } else {
                self.sellerView?.pushDataView(response!)
            }
        })
    }
}
