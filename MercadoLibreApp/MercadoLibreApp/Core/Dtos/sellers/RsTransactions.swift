//
//  RsTransactions.swift
//  MercadoLibreApp
//
//  Created by PersonalSoft PS on 10/04/21.
//

import Foundation

struct RsTransactions: Codable {
    let canceled, completed: Int
    let ratings: RsRatings
    let total: Int
}
