//
//  RsRatings.swift
//  MercadoLibreApp
//
//  Created by PersonalSoft PS on 10/04/21.
//

import Foundation

struct RsRatings: Codable {
    let negative: Double
    let neutral: Double
    let positive: Double
}
