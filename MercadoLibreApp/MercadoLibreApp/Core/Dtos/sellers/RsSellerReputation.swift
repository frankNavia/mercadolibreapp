//
//  RsSellerReputation.swift
//  MercadoLibreApp
//
//  Created by PersonalSoft PS on 10/04/21.
//

import Foundation

struct RsSellerReputation: Codable {
    let powerSellerStatus: String?
    let transactions: RsTransactions

    enum CodingKeys: String, CodingKey {
        case powerSellerStatus = "power_seller_status"
        case transactions
    }
}
