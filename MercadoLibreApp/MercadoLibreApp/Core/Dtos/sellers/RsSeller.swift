//
//  RsSeller.swift
//  MercadoLibreApp
//
//  Created by PersonalSoft PS on 10/04/21.
//

import Foundation

struct RsSeller: Codable {
    let id: Int
    let nickname, registrationDate, countryID: String
    let tags: [String]
    let logo: String?
    let points: Int
    let permalink: String
    let sellerReputation: RsSellerReputation

    enum CodingKeys: String, CodingKey {
        case id, nickname
        case registrationDate = "registration_date"
        case countryID = "country_id"
        case tags, logo, points, permalink
        case sellerReputation = "seller_reputation"
    }
}
