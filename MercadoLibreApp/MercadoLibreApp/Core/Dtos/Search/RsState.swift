//
//  RsCity.swift
//  MercadoLibreApp
//
//  Created by PersonalSoft PS on 10/04/21.
//

import Foundation

struct RsState: Codable {
    let id: String?
    let name: String
}
