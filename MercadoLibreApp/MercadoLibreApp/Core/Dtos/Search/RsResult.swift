//
//  RsResult.swift
//  MercadoLibreApp
//
//  Created by PersonalSoft PS on 10/04/21.
//

import Foundation

struct RsResult: Codable {
    let id, title: String
    let seller: RsSellerId
    let price: Double
    let currencyID: String
    let availableQuantity, soldQuantity: Int
    let stopTime: String
    let condition: String?
    let permalink: String
    let thumbnail: String
    let acceptsMercadopago: Bool
    let sellerAddress: RsSellerAddress
    let attributes: [RsAttribute]?
    let categoryID: String
    let officialStoreID: Int?
    let catalogProductID: String?
    let tags: [String]?

    enum CodingKeys: String, CodingKey {
        case id
        case title, seller, price
        case currencyID = "currency_id"
        case availableQuantity = "available_quantity"
        case soldQuantity = "sold_quantity"
        case stopTime = "stop_time"
        case condition, permalink, thumbnail
        case acceptsMercadopago = "accepts_mercadopago"
        case sellerAddress = "seller_address"
        case attributes
        case categoryID = "category_id"
        case officialStoreID = "official_store_id"
        case catalogProductID = "catalog_product_id"
        case tags
    }
}
