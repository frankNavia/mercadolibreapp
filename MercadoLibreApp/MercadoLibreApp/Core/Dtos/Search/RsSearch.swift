//
//  RsSearch.swift
//  MercadoLibreApp
//
//  Created by PersonalSoft PS on 10/04/21.
//

import Foundation

struct RsSearch: Codable {
    let results: [RsResult]
}
