//
//  RsAttribute.swift
//  MercadoLibreApp
//
//  Created by PersonalSoft PS on 10/04/21.
//

import Foundation

struct RsAttribute: Codable {
    let name, attributeGroupName: String
    let valueName: String?
    
    enum CodingKeys: String, CodingKey {
        case name
        case valueName = "value_name"
        case attributeGroupName = "attribute_group_name"
    }
}
