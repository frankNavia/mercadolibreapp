//
//  RsSellerAddress.swift
//  MercadoLibreApp
//
//  Created by PersonalSoft PS on 10/04/21.
//

import Foundation

struct RsSellerAddress: Codable {
    let id, comment, addressLine, zipCode: String
    let country, state, city: RsState
    let latitude, longitude: String

    enum CodingKeys: String, CodingKey {
        case id, comment
        case addressLine = "address_line"
        case zipCode = "zip_code"
        case country, state, city, latitude, longitude
    }
}
