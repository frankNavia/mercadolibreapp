//
//  RsCurrencyConvert.swift
//  MercadoLibreApp
//
//  Created by PersonalSoft PS on 10/04/21.
//

import Foundation

struct RsCurrencyConvert: Codable {
    let currencyBase, currencyQuote: String
    let ratio, rate, invRate: Double
    let creationDate, validUntil: String

    enum CodingKeys: String, CodingKey {
        case currencyBase = "currency_base"
        case currencyQuote = "currency_quote"
        case ratio, rate
        case invRate = "inv_rate"
        case creationDate = "creation_date"
        case validUntil = "valid_until"
    }
}
