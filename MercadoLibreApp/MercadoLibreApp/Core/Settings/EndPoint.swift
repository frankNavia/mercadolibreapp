//
//  SearchItem.swift
//  MercadoLibreApp
//
//  Created by PersonalSoft PS on 10/04/21.
//

import Foundation
class EndPoint {
    let urlBase = "https://api.mercadolibre.com/"
    let searchEndPoint = "sites/MLA/search?q=%@&sort=price_desc"
    let currencyConvertEndPoint = "currency_conversions/search?from=%@&to=COP"
    let sellerDataEndPoint = "users/%@"
}
