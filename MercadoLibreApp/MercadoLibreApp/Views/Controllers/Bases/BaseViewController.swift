//
//  BaseViewController.swift
//  MercadoLibreApp
//
//  Created by PersonalSoft PS on 12/04/21.
//

import UIKit
import Foundation

extension UIImageView {
    func loadImage(url: URL) {
        DispatchQueue.global().async { [weak self] in
            if let data = try? Data(contentsOf: url),
            let image = UIImage(data: data) {
                DispatchQueue.main.async {
                    self?.image = image
                }
            }
        }
    }
}

extension UIViewController {
    func showError(error: String) {
        let alert = UIAlertController(title: nil, message: error, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
        self.present(alert, animated: true)
    }
    
    func addImageToTitle() {
        let sizNavigation = navigationController!.navigationBar.frame.size
        let image = UIImage(named: "Isotipo.png")
        let imageView = UIImageView(image: image)
        imageView.frame = CGRect(x: 0, y: 0, width: sizNavigation.width / 2, height: sizNavigation.height)
        imageView.contentMode = .scaleAspectFit
        navigationItem.titleView = imageView
    }
    
    func shareUrl(urlString : String) {
        if let name = URL(string: urlString), !name.absoluteString.isEmpty {
            let objectsToShare = [name]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            self.present(activityVC, animated: true, completion: nil)
        } else {
            showError(error: "El dispositivo no tiene aplicaciones compatible")
        }
    }
    
    func openUrl(urlString : String) {
        if let url = URL(string: urlString) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:], completionHandler:nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        } else {
            showError(error: "El dispositivo no tiene browser compatible")
        }
    }
}
