//
//  SearchViewController.swift
//  MercadoLibreApp
//
//  Created by PersonalSoft PS on 10/04/21.
//

import UIKit

class SearchViewController: UIViewController {

    var productsArr:[ProductItemModel]?
    
    @IBOutlet weak var _btnSearch: UIButton!
    
    @IBOutlet weak var _actIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var _vwData: UIView!
    
    @IBOutlet weak var _tfSearch: UITextField!
    
    @IBOutlet weak var _tbResponse: UITableView!
    
    fileprivate let searchPresenter = SearchPresenter()
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addImageToTitle()
        searchPresenter.attachView(view: self)
        
        _btnSearch.addTarget(self, action: #selector(searchProduct), for: .touchUpInside)
        
        _tbResponse.register(UINib(nibName: ProductItemViewCell.identifier, bundle: nil), forCellReuseIdentifier: ProductItemViewCell.identifier)
        _tbResponse.dataSource = self
        _tbResponse.delegate = self
        _tfSearch.delegate = self
        
        _vwData.isHidden = true
        _actIndicator.isHidden = true
        _actIndicator.hidesWhenStopped = true
    }
    
    @objc func searchProduct(sender: UIButton!) {
        let textCount: Int = _tfSearch.text!.count
        if (textCount > 2) {
            _tfSearch.resignFirstResponder()
            searchPresenter.searchProducts(text: _tfSearch.text!)
        }
    }
}

extension SearchViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

extension SearchViewController: SearchView {
    func pushError(_ messageError: String) {
        DispatchQueue.main.async {
            self.showError(error: messageError)
            self._actIndicator.stopAnimating()
        }
    }
    
    func pushDataView(_ products: [ProductItemModel]) {
        self.productsArr = products
        
        DispatchQueue.main.async {
            self._vwData.isHidden = false
            self._tbResponse.reloadData()
            self._actIndicator.stopAnimating()
        }
    }
    
    func startActivity() {
        DispatchQueue.main.async {
            self._actIndicator.isHidden = false
            self._actIndicator.startAnimating()
        }
    }
}

extension SearchViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count = productsArr?.count;
        return count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item:ProductItemModel = productsArr![indexPath.row]
        let cell:ProductItemViewCell = tableView.dequeueReusableCell(withIdentifier: ProductItemViewCell.identifier, for: indexPath) as! ProductItemViewCell
        cell.setData(item: item)
        return cell
    }
}

extension SearchViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item:ProductItemModel = productsArr![indexPath.row]
        let nextController = ProductDetailsViewController(product: item)
        navigationController?.pushViewController(nextController, animated: true)
    }
}
