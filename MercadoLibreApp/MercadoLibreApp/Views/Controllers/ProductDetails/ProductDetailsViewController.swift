//
//  ProductDetailsViewController.swift
//  MercadoLibreApp
//
//  Created by PersonalSoft PS on 10/04/21.
//

import UIKit

class ProductDetailsViewController: UIViewController {

    @IBOutlet weak var _lblCondition: UILabel!
    
    @IBOutlet weak var _lblTitle: UILabel!
    
    @IBOutlet weak var _imgProduct: UIImageView!
    
    @IBOutlet weak var _btnShare: UIButton!
    
    @IBOutlet weak var _lblPrice: UILabel!
    
    @IBOutlet weak var _indPrice: UIActivityIndicatorView!
    
    @IBOutlet weak var _vwSeller: UIView!
    
    @IBOutlet weak var _lblSellerName: UILabel!
    
    @IBOutlet weak var _lblSold: UILabel!
    
    @IBOutlet weak var _lblLocation: UILabel!
    
    @IBOutlet weak var _indSeller: UIActivityIndicatorView!
    
    @IBOutlet weak var _lblAvalaible: UILabel!
    
    @IBOutlet weak var _lblAributes: UILabel!
    
    @IBOutlet weak var _vwAttributes: UIView!
    
    var productSelected: ProductItemModel
    var sellerData: SellerModel?
    
    fileprivate let currencyPresenter = CurrencyPresenter()
    fileprivate let sellerPresenter = SellerPresenter()
    
    init(product: ProductItemModel) {
        productSelected = product
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        currencyPresenter.attachView(view: self)
        sellerPresenter.attachView(view: self)
        
        currencyPresenter.convertPriceToCurrency(currencyID: productSelected.currencyID)
        sellerPresenter.getSellerFromID(sellerID: productSelected.sellerID)
        
        addImageToTitle()
        validateAttributes()

        let url = NSURL(string: productSelected.thumbnail)
        _imgProduct.loadImage(url: url! as URL)
        
        _lblCondition.text = "\(productSelected.condition) | \(productSelected.soldQuantity) vendidos"
        _lblTitle.text = productSelected.title
        _lblLocation.text = productSelected.address
        _lblAvalaible.text = "\(productSelected.availableQuantity) disponibles"
        
        _lblSellerName.isUserInteractionEnabled = true
        _lblSellerName.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(openSellerWebUrl)))
        
        _btnShare.layer.cornerRadius = _btnShare.bounds.size.width * 0.5
        _btnShare.addTarget(self, action: #selector(shareProductUrl), for: .touchUpInside)
    }
    
    private func validateAttributes() {
        var attributes = ""
        for item in productSelected.attributes ?? [] {
            attributes = attributes + "* \(item) \n\n"
        }
        
        _lblAributes.text = attributes
        
        if attributes.isEmpty {
            _vwAttributes.isHidden = true
        }
    }

    @objc func shareProductUrl(sender: Any) {
        shareUrl(urlString: productSelected.mercadoLink)
    }
    
    @objc func openSellerWebUrl(sender: Any) {
        openUrl(urlString: sellerData!.permalink)
    }
}

extension UIViewController {
    open override func awakeFromNib() {
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
}

extension ProductDetailsViewController: CurrencyView, SellerView {
    func pushDataView(_ currency: CurrencyModel) {
        let price = self.productSelected.price * currency.ratio
        let priceString = MoneyHelper.convertPrice(priceValue: price)
        
        DispatchQueue.main.async {
            self._lblPrice.text = "$ \(priceString)"
            self._lblPrice.isHidden = false
            self._indSeller.stopAnimating()
        }
    }
    
    func pushDataView(_ seller: SellerModel) {
        self.sellerData = seller
        let sold = "\(seller.reputation.status) | \(seller.reputation.totalTransactions) transacciones"
        
        DispatchQueue.main.async {
            self._lblSellerName.text = seller.name
            self._lblSold.text = sold
            
            self._vwSeller.isHidden = false
            self._indSeller.stopAnimating()
        }
    }
    
    func pushError(_ messageError: String) {
        DispatchQueue.main.async {
            if self._lblPrice.isHidden {
                self._indPrice.stopAnimating()
            }
            
            if self.sellerData == nil {
                self._indSeller.stopAnimating()
            }
            
            self.showError(error: messageError)
        }
    }
    
    func startActivity() {
        DispatchQueue.main.async {
            self._lblPrice.isHidden = true
            self._indPrice.isHidden = true
            self._indPrice.hidesWhenStopped = true
            
            self._vwSeller.isHidden = true
            self._indSeller.isHidden = true
            self._indSeller.hidesWhenStopped = true
        }
    }
}
