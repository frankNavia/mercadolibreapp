//
//  MoneyHelper.swift
//  MercadoLibreApp
//
//  Created by PersonalSoft PS on 12/04/21.
//

import Foundation

class MoneyHelper {
    static func convertPrice(priceValue: Double) -> String {
        let currencyFormatter = NumberFormatter()
        currencyFormatter.usesGroupingSeparator = true
        currencyFormatter.numberStyle = .currency
        currencyFormatter.currencySymbol = ""
        currencyFormatter.locale = Locale(identifier: "co_CO")
        
        return currencyFormatter.string(from: NSNumber(value: priceValue)) ?? "error"
    }
}
