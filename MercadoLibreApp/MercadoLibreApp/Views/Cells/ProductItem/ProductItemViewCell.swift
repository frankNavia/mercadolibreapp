//
//  ProductItemViewCell.swift
//  MercadoLibreApp
//
//  Created by PersonalSoft PS on 10/04/21.
//

import UIKit

class ProductItemViewCell: UITableViewCell {

    static var identifier: String {
        return "ProductItemViewCell"
    }
    
    @IBOutlet weak var _imgProduct: UIImageView!
    
    @IBOutlet weak var _lblName: UILabel!
    
    @IBOutlet weak var _lblCondition: UILabel!
    
    func setData(item: ProductItemModel) {
        _lblName.text = item.title
        _lblCondition.text = item.condition
        
        let url = NSURL(string: item.thumbnail)
        self._imgProduct.loadImage(url: url! as URL)
    }
}
