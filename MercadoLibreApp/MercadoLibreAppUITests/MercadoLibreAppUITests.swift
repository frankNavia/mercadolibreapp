//
//  MercadoLibreAppUITests.swift
//  MercadoLibreAppUITests
//
//  Created by PersonalSoft PS on 10/04/21.
//

import XCTest

class MercadoLibreAppUITests: XCTestCase {

    var app: XCUIApplication!

    // MARK: - XCTestCase

    override func setUp() {
        super.setUp()

        continueAfterFailure = false

        app = XCUIApplication()
    }
    
    // MARK: - Tests

    func testErrorFilter() {
        app.launch()

        XCTAssertTrue(app.isDisplaySearchView)
        XCTAssertTrue(app.textFields["txFilter"].exists)
        XCTAssertTrue(app.buttons["btnFilter"].exists)
        
        app.textFields["txFilter"].tap()
        app.textFields["txFilter"].typeText("qaserftgv")
        app.buttons["btnFilter"].tap()
        
        XCTAssertTrue(app.alerts.element.staticTexts["No hay coincidencia de datos"].exists)
    }
    
    func testSelectProduct() {
        app.launch()

        XCTAssertTrue(app.isDisplaySearchView)
        
        app.textFields["txFilter"].tap()
        app.textFields["txFilter"].typeText("motorola g9")
        app.buttons["btnFilter"].tap()
    
        XCTAssertTrue(tableView.waitForExistence(timeout: 1))
        XCTAssertTrue(tableView.cells.count > 0)
        
        tableView.cells.element(boundBy: 0).tap()
        
        XCTAssertFalse(app.isDisplaySearchView)
        XCTAssertTrue(app.isDisplayProductDetailsView)
        XCTAssertTrue(app.staticTexts["lblCondition"].exists)
        XCTAssertTrue(app.images["imgProduct"].exists)
    }

    // MARK: - Helpers
    
    var tableView: XCUIElement {
        return app.tables["tblFilter"]
    }
}
