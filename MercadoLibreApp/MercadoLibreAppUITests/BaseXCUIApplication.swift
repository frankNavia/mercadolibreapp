//
//  BaseXCUIApplication.swift
//  MercadoLibreAppUITests
//
//  Created by PersonalSoft PS on 12/04/21.
//

import XCTest

extension XCUIApplication {
    var isDisplaySearchView: Bool {
        return otherElements["SearchView"].exists
    }
    
    var isDisplayProductDetailsView: Bool {
        return otherElements["ProductDetailsView"].exists
    }
}
