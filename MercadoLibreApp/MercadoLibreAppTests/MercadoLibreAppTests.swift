//
//  MercadoLibreAppTests.swift
//  MercadoLibreAppTests
//
//  Created by PersonalSoft PS on 10/04/21.
//

import XCTest
@testable import MercadoLibreApp

class MercadoLibreAppTests: XCTestCase {
    
    // MARK: - Tests

    func testSearchMapper() throws {
        let url = Bundle(for: MercadoLibreAppTests.self).url(forResource: "searchResponse", withExtension: "json")
        let data = try Data(contentsOf: url!)
        let rsSearch = try JSONDecoder().decode(RsSearch.self, from: data)
        XCTAssertTrue(rsSearch.results.count > 0)
    }
    
    func testSellerMapper() throws {
        let url = Bundle(for: MercadoLibreAppTests.self).url(forResource: "sellerResponse", withExtension: "json")
        let data = try Data(contentsOf: url!)
        let rsSeller = try JSONDecoder().decode(RsSeller.self, from: data)
        XCTAssertEqual(rsSeller.nickname, "NORTE TECNOLOGIA")
    }
    
    func testCurrencyMapper() throws {
        let url = Bundle(for: MercadoLibreAppTests.self).url(forResource: "currencyResponse", withExtension: "json")
        let data = try Data(contentsOf: url!)
        let rsCurrency = try JSONDecoder().decode(RsCurrencyConvert.self, from: data)
        XCTAssertEqual(rsCurrency.currencyQuote, "COP")
    }
    
    func testConvertPriceSuccess() {
        let priceDouble = 123456.0
        let priceString = MoneyHelper.convertPrice(priceValue: priceDouble)
        XCTAssertEqual(priceString, " 123.456")
    }
}
