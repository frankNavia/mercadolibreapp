Este repositorio contiene una aplicacion simple utilizando la arquitectura MVP con el patron Delegate y Repository, apuntando a servicios restful de Mercado Libre. 
Cuenta con dos vistas: Primera se encarga de consultar y visualizar la información inicial de productos. Por ultimo, al seleccionar un producto de la lista, se visualiza una nueva vista con información completa del producto seleccionado.

Pre-requisitos 📋
iOS 10+
Xcode 11.1+
Swift 5.1+
Cocoa-Pods 
